require 'test_helper'

class LikeControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get like_show_url
    assert_response :success
  end

  test "should get create" do
    get like_create_url
    assert_response :success
  end

  test "should get update" do
    get like_update_url
    assert_response :success
  end

end
