<h2>Sistema de Votação</h2>

O objetivo desse artigo é desenvolver, através de um exemplo prático, um sistema de votação no *framework* Ruby on Rails.

<h2>Ferramentas</h2>

1. Ruby 2.5
2. Ruby on Rails 5.2
3. yarn <https://onebitcode.com/instalando-pacotes-no-rails-com-yarn-jquery-bootstrap-e-materialize/>
4. Postgres
5. Devise
6. Rails admin
7. Active Storage

<h2>O que é o app?</h2>

A grande parte dos sistemas de votação é baseada na regra da [maioria](https://pt.wikipedia.org/wiki/Maioria_simples), ou seja, o princípio de que deve ser satisfeita a opinião apoiada por mais da metade dos votantes. O sistema de votação que iremos desenvolver contabilizará a quantidade de likes e dislikes de uma campanha.

<h2>O que vamos criar?</h2>

O sistema possuirá um administrativo e um sistema de votação. Cada tipo de usuário terá uma função bem definida no app. A função do administrador é criar, bloquear, editar e excluir a campanha enquanto o usuário, tem o papel de votar se gostou ou não gostou de uma determinada campanha.

Para desenvolvimento das telas de votação o sistema contará com o framework bulma para auxiliar no desenvolvimento e no administrativo a gem do rails admin criará uma interface para que você possa gerenciar seus dados. Para uma explicação mais aprofundada dê uma olhada na página da Gem no GitHub [clicando aqui](https://github.com/sferik/rails_admin)!. O Devise será responsável pelo controle de sessão e para salvar as imagens das campanha será utilizado o Active Storage.  

<h2>Passo a Passo da criação do APP</h2>

1. Agora vamos criar um novo projeto com o nome **voting**.

   ```bash
   rails new voting --database=postgresql
   ```

2. Entre na pasta do projeto que foi criado.

   ```bash
   cd voting
   ```

3. Adicione no Gemfile:

   ```ruby
   gem 'devise'
   gem 'rails_admin', '~> 1.4', '>= 1.4.2'
   ```

4. Instale as gems com o seguinte comando:

   ```bash
   bundle install
   ```

5. O arquivo **package.json** deve ficar com o seguinte conteúdo:

   ```json
   {
     "name": "voting",
     "private": true,
     "dependencies": {
       "@fortawesome/fontawesome-free": "^5.8.1",
       "@rails/webpacker": "3.5",
       "bulma": "0.7.2",
       "jquery": "^3.3.1"
     },
     "devDependencies": {
       "webpack-dev-server": "2.11.2"
     }
   }
   ```

   > O Yarn é um gerenciador de pacotes javascript rápido, seguro e confiável que foi integrado no rails > 5.1 para facilitar ainda mais a gestão das dependências. Assim como o Bundler o Yarn possui um arquivo onde as dependências são declaradas chamado **package.json** 

6. Para instalar as dependências listadas no package.json:

   ```bash
   yarn install
   ```

7. Para instalar o jQuery:

   ```bash
   yarn add jquery
   ```

8. Para importar, deixe o arquivo _app/assets/javascripts/application.js_ com o seguinte conteúdo:

   ```
   //= require rails-ujs
   //= require activestorage
   //= require turbolinks
   //= require jquery/dist/jquery.min
   //= require_tree .
   ```

9. Instale o pacote de fontes Font Awesome.

   ```bash
   yarn add @fortawesome/fontawesome-free
   ```

10. Renomeie o arquivo application.css para application.scss:

    ```bash
      mv app/assets/stylesheets/application.css app/assets/stylesheets/application.scss
    ```

11. Para realizar os imports o arquivo app/assets/stylesheets/application.scss deve ficar com o seguinte conteúdo:

    ```scss
    @import "bulma/bulma";
    @import "@fortawesome/fontawesome-free/css/all.css";
    ```

12. Para instalar o Active Storage (gerar a migration dele) rode no console:

    ```bash
    rails active_storage:install
    ```

13. Para configurar o devise no seu app execute o seguinte comando no terminal:

    ```ruby
    rails generate devise:install
    ```

14. Para instalar o Rails Admin rode no console.

    ```bash
    rails g rails_admin:install
    ```

15. Ao rodar o comando, o Rails Admin perguntará em qual rota você gostaria de instalar. Neste tutorial deixaremos em /admin como no default, **para isso aperte enter**, mas você poderá escolher outra rota ou até mesmo instalar na home.

    ```bash
    Where do you want to mount rails_admin? Press  for [admin]
    ```

16. Vamos gerar os modelos do sistema.

    ```ruby
    rails generate devise user name:string
    
    rails generate migration add_admin_to_users admin:boolean
    
    rails generate model campaign title description:text user:references blocked:boolean
    
    rails generate model like campaign:references user:references kind:boolean
    ```

17. Para configurar o acesso ao administrativo substitua o arquivo **config/initializers/rails_admin.rb** por:

    ```ruby
    RailsAdmin.config do |config|
      
      config.authenticate_with do
        warden.authenticate! scope: :user
      end
      config.current_user_method(&:current_user)
    
      config.authorize_with do
        redirect_to main_app.root_path unless current_user.admin?
      end
      
      config.actions do
        dashboard                     # mandatory
        index                         # mandatory
        new
        export
        bulk_delete
        show
        edit
        delete
        show_in_app
      end
    end
    ```

    > Foi utilizado uma variável do tipo boolean para identificar se o usuário é administrador, para o Railsadmin liberar o acesso correto ao user admin, as seguintes configurações realizam essas tarefas: com config.authenticate_with informamos ao RailsAdmin que o usuário tem que está devidamente autenticado e config.authorize_with informamos que o usuário tem que ser do tipo admin para ter a autorização de usar o administrativo.

18. Vamos criar um usuário, no diretorio **db/seeds.rb**, para acessar o administrativo adicione:

    ```ruby
    User.create(email: 'adm@email.com', name:'Administrador', password: '12345678', password_confirmation: '12345678', admin: true)
    ```

19. Para criar o banco de dados e as tabelas do app rode no terminal o seguinte comando:

    ```ruby
    rake db:create db:migrate db:seed
    ```

20. Adicione ao modelo user em **app/models/user.rb**.

    ```ruby
    has_many :campaigns
    has_many :likes
    validates_presence_of :name 
    ```

21. Em **app/models/campaign.rb** substitua:

    ```ruby
    class Campaign < ApplicationRecord
      #Para utilizar o Active Storage basta adicionar attached ao model
      has_one_attached :image
      
      belongs_to :user
      has_many :likes
    
      validates_presence_of :title, :description
    
      before_update :is_blocked
    
      private
      
      # verifica se a campanha está bloqueada e aborta o update da Campaing
      def is_blocked
        campaign = Campaign.find(self.id)
        if campaign.blocked
          throw(:abort)
        end
      end
    
    end
    ```

    > No model Campaign é onde utilizaremos o Active Storage para armazenar as fotos, para isso adicione ao modelo  has_one_attached.
    >
    > Campaign pertence ao usuário, para criar um modelo que pertence a outro modelo utiliza-se o **references**. ex: *rails g model campaign user:references*
    >
    > O modelo campaign pode possuir mais de um like, então para funcionar corretamente utilizamos o **has_many: likes**.
    >
    > Utilizamos **validates_presence_of** para informar que aquele atributo não pode ser nulo. 
    >
    > Foi utlizado um *callback* **before_update**, o método is_blocked foi criado para verificar se aquela campaign está bloqueada, se a campanha estiver bloqueada será abortado todo o processo de update. Com esse método conseguimos garantir que não ocorrerá nenhuma alteração na campanha depois do bloqueio da mesma.

22. Substitua o código do modelo like em **app/models/like.rb**:

    ```ruby
    class Like < ApplicationRecord
      belongs_to :campaign
      belongs_to :user
    
      before_update :is_blocked
    
      private
      
      # verifica se a campanha está bloqueada e aborta o update do Like
      def is_blocked
        if self.campaign.blocked
          throw(:abort)
        end
      end
    end
    ```

    > Nesse modelo também temos um *callback* before_update, o método is_blocked foi criado para verificar se  à campaign que usuário está tentando votar apresenta-se bloqueada, se a campanha estiver bloqueada nenhum usuário irá conseguir realizar a votação.

23. Crie um controller com o nome campaigns com os métodos index e show usando o seguinte comando:

    ```ruby
    rails generate controller campaigns index show
    ```

24. Para criar o controlador do like rode no terminal:

    ```
    rails generate controller like
    ```

25. Substitua o código de **app/controllers/applications_controller.rb** por:

    ```ruby
    class ApplicationController < ActionController::Base
      before_action :configure_permitted_parameters, if: :devise_controller?
      before_action :authenticate_user!
    
      protected
      # permitir adicionar o atributo name através do metodo do devise sign_up
      def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
      end
    
    end
    ```

    > Como adicionamos o atributo name, precisamos informar para o devise que o método sing_up irá receber um parâmetro novo, para isso adicionamos o método *configure_permitted_parameters*.

26. Troque o código do arquivo **app/controllers/campaigns_controller.rb** por esse:

    ```ruby
    class CampaignsController < ApplicationController
      before_action :set_campaigns, only: [:show]
    
      def index
        @campaigns = Campaign.all
      end
    
      def show
        set_likes_count if @campaign.blocked
      end
    
      private 
    
      def set_campaigns
        @campaign = Campaign.find(params[:id])
      end
    
      def set_likes_count
        @likes = @campaign.likes.where(kind: true).count
        @dislikes = @campaign.likes.where(kind: false).count
      end
    
    end
    
    ```

27. Substitua o código do arquivo **app/views/campaigns/index.html.erb** por esse:

    ```erb
    <section id="dashboard" class="section has-text-white has-text-centered dashboard">
    
      <div class="campaigns">
        <h2 class="is-size-5 has-text-white-semibold">All Campaigns</h2>
        <% if @campaigns.count > 0 %>
          <br/>
          <div class="columns is-multiline is-mobile">
            <%= render @campaigns %>
          </div>
        <% end %>
      </div>
    
    
    </section>
    ```

    > Esse código será responsável por mostrar todas as campanhas sendo que a variável @campaigns foi instanciada no método index do controller campaign.

28. Substitua o código do arquivo **app/views/campaigns/show.html.erb** por:

    ```erb
    
    <section class="container">
        <div class="columns features">
            <div class="column is-8">
                <div class="card is-shady">
          
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <%= image_tag @campaign.image %>
                        </figure>
                    </div>
    
                    <div class="card-content">
                        <div class="content">
                            <h2 class="title has-text-centered"><%= @campaign.title %></h2>
                            <p><%= @campaign.description %></p>
                            <time datetime="2016-1-1"><%= @campaign.created_at %></time>
                        </div>
                    </div>
                    <% if @campaign.blocked %>
                        <div class="has-text-centered">
                            <i><%= @likes %> gostaram</i>
                        </div>
                        <div class="has-text-centered">
                            <i><%= @dislikes %> não gostaram</i>
                        </div>
                    <% else %>
                        <div class="has-text-centered">
                            <i id="liked"></i>
                            <i id="disliked"></i>
                        </div>
                    <% end %>
                </div>
            </div>
        </div>
    </section>
    
    ```

    > Esse código será responsável por mostrar de forma detalhada uma determinada campaign.

29. Crei um arquivo **_campaign.html.erb** em **app/views/campaigns** e adicione:

    ```erb
    <div class="column is-full-mobile is-one-quarter-desktop">
    
        <%= link_to campaign do %>
            <section class="container">
                <div class="columns features">
                    <div class="column is-4">
                        <div class="card is-shady">
                                <% if campaign.blocked %>
                                   <i>Bloqueado!</i>
                                <% end %>
                                <div class="card-image">
                                    <figure class="image is-4by3">
                                        <%= image_tag campaign.image %>
                                    </figure>
                                </div>
                                <div class="card-content">
                                    <div class="content">
                                        <h4><%= campaign.title %></h4>
                                        <p><%= campaign.description %></p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </section>
        <% end %>
    
    </div>
    
    ```

    > Aqui será executado pelo código presente no index.html, *<%= render @campaigns %>*. Nesta parte ocorrerá a criação, com o auxílio do framework Bulma, dos cards da campaing.

30. Nesta parte iremos customizar as telas do devise para isso vamos gerar as views do devise.

    ```ruby
    rails generate devise:views
    ```

31. Em **app/views/devise/registrations/new.html.erb** troque por:

    ```erb
    <section class="section">
      <div class="container column is-half">
        <h1 class="is-size-3 is-size-4-mobile has-text-white-semibold has-text-white has-text-centered">Sign up</h1>
      </div>
      <div class="container column is-half login">
        <form class="new_user" id="new_user" action="/users" accept-charset="UTF-8" method="post">
          <%= hidden_field_tag :authenticity_token, form_authenticity_token %>
          <%= devise_error_messages! %>
          <div class="field">
            <p class="control has-icons-left">
              <input autofocus="autofocus" class="input" placeholder="Full name" autocomplete="name" type="text" name="user[name]" id="user_name" >
              <span class="icon is-small is-left">
                <i class="fa fa-user"></i>
              </span>
            </p>
          </div>
          <div class="field">
            <p class="control has-icons-left has-icons-right">
              <input class="input" placeholder="Email" autofocus="autofocus" autocomplete="email" type="email" value="" name="user[email]" id="user_email">
              <span class="icon is-small is-left">
                <i class="fas fa-envelope"></i>
              </span>
              <span class="icon is-small is-right">
                <i class="fas fa-check"></i>
              </span>
            </p>
          </div>
          <div class="field">
            <p class="control has-icons-left">
              <input class="input" placeholder="Password" autocomplete="current-password" type="password" name="user[password]" id="user_password" >
              <span class="icon is-small is-left">
                <i class="fas fa-lock"></i>
              </span>
            </p>
          </div>
          <div class="field">
            <p class="control has-icons-left">
              <input class="input" placeholder="Password Confirmation" autocomplete="current-password" type="password" name="user[password_confirmation]" id="user_password_confirmation" >
              <span class="icon is-small is-left">
                <i class="fas fa-lock"></i>
              </span>
            </p>
          </div>
          <div class="field">
            <p class="control center">
              <button class="button is-fullwidth is-rounded is-primary" value="Sign up" type="submit" name="commit">
                Sign up
              </button>
            </p>
          </div>
        </form>
      </div>
      <div class="container column is-half center">
        <br/>
        <a class="button is-fullwidth is-rounded is-outlined is-link" href="/users/sign_in">Login</a><br />
      </div>
    </section
    ```

32. Em **app/views/devise/sessions/new.html.erb** troque por:

    ```erb
    <section  class="section">
      <div class="container column is-half">
        <h1 class="is-size-3 is-size-4-mobile has-text-white-semibold has-text-white has-text-centered">Login</h1>
      </div>
      <div class="container column is-half login">
        <form class="new_user" id="new_user" action="/users/sign_in" accept-charset="UTF-8" method="post">
          <%= hidden_field_tag :authenticity_token, form_authenticity_token %>
          <%= devise_error_messages! %>
          <div class="field">
            <p class="control has-icons-left has-icons-right">
              <input autofocus="autofocus" class="input" placeholder="Email" autofocus="autofocus" autocomplete="email" type="email" value="" name="user[email]" id="user_email">
              <span class="icon is-small is-left">
                <i class="fas fa-envelope"></i>
              </span>
              <span class="icon is-small is-right">
                <i class="fas fa-check"></i>
              </span>
            </p>
          </div>
          <div class="field">
            <p class="control has-icons-left">
              <input class="input" placeholder="Password" autocomplete="current-password" type="password" name="user[password]" id="user_password" >
              <span class="icon is-small is-left">
                <i class="fas fa-lock"></i>
              </span>
            </p>
          </div>
          <div class="field">
            <input name="user[remember_me]" type="hidden" value="0" /><input type="checkbox" value="1" name="user[remember_me]" id="user_remember_me" />
            <label for="user_remember_me">Remember me</label>
          </div>
          <div class="field">
            <p class="control center">
              <button class="button is-fullwidth is-rounded is-link" type="submit" name="commit">
                Login
              </button>
            </p>
          </div>
        </form>
      </div>
      <div class="container column is-half center">
        <br/>
        <a class="button is-fullwidth is-rounded is-outlined is-primary" href="/users/sign_up">Sign up</a>
        <a class="button is-fullwidth is-rounded is-outlined is-warning" href="/users/password/new">Forgot your password?</a>
      </div>
    </section>
    
    ```

33. Vamos criar uma pasta chamada **shared** em **app/views/** e um arquivo dentro da pasta chamado **_menu.html.erb** e dentro do arquivo coloque:

    ```erb
    <nav class="navbar" role="navigation" aria-label="main navigation">
      <div class="navbar-brand">
        <a class="navbar-item" href="https://bulma.io">
          <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
        </a>
    
        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
    
      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
          <a class="navbar-item" href="/">
            Home
          </a>
          <% if current_user && current_user.admin %>
            <a class="navbar-item" href="/admin">
              Administrativo
            </a>
          <% end %>
        </div>
    
        <div class="navbar-end">
          <div class="navbar-item">
            <div class="buttons">
              <% if !current_user %>
                <%= link_to('Log in', new_user_session_path, :class => "button is-info") %>               
                <%= link_to('Sign up', new_user_registration_path, :class => "button is-primary") %> 
              <% end %>
              <% if current_user %>
                <%= link_to('Logout', destroy_user_session_path, method: :delete, :class => "button is-light") %>        
              <% end %>
            </div>
          </div>
        </div>
      </div>
    </nav>
    ```

    > Esse código é resposável por criar menu do sistema com as opções: Home, Log in, Sign up, Logout e o administrativo caso o usuário seja o administrador.

34. Atualize o arquivo **app/views/layouts/application.html.erb** com:

    ```erb
    <!DOCTYPE html>
    <html>
      <head>
        <title>Vote</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%= csrf_meta_tags %>
        <%= csp_meta_tag %>
        <%= stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track': 'reload' %>
        <%= javascript_include_tag 'application', 'data-turbolinks-track': 'reload' %>
      </head>
    
      <body>
        <%= render "shared/menu" %>
        <%= yield %>
      </body>
    </html>
    ```

35. Agora vamos ajustar as views alterando o scss, em **app/assets/stylesheets/application.scss** troque por:

    ```scss
    @import "bulma/bulma";
    @import "@fortawesome/fontawesome-free/css/all.css";
    @import "./**/*";
    
    html{
      background-color: $dark;
    }
    
    ```

    > O @import "./**/*"; está importanto todos os arquivos que esteja dentro da pasta *stylesheets*

36. Crie um arquivo **devise.scss** em  **app/assets/stylesheets/** e adicione:

    ```scss
    body {
      margin-top: 10px;
      margin-left: 10px;
      margin-right: 10px;
    }
    .login {
      margin-top: 10px;
    }
    .title{
      color: white
    }
    .center{
      text-align: center;
    }
    .label{
      color: white
    }
    .is-link{
      color: white
    }
    .field{
      color: white
    }
    .button{
      margin-top: 10px;
    }
    .a{
      margin-top: 10px;
    }
    ```

37. Em  **app/assets/stylesheets/campaigns** coloque:

    ```scss
    .card-backgroud{
        background-color: $grey-dark;
        border-radius: 5px;
    }
    ```

38. O usuário irá realizar a votação na tela show da campaign e para realizar a requisição usaremos js, em  **app/assets/javascripts/** crie um arquivo campaigns.js e coloque:

    ```javascript
    $(document).on('ready turbolinks:load', function(){
    
        var voidLiked = `<i class="far fa-grin-beam fa-2x" style="color:#D5D3D3"></i>`;
        var liked = `<i class="fas fa-grin-beam fa-2x"></i>`;
        var disliked = `<i class="fas fa-angry fa-2x"></i>`;
        var voidDisliked = `<i class="far fa-angry fa-2x" style="color:#D5D3D3"></i>`;
        
        var campaign_id = $(location).attr('href').split("/").pop();
        var like_id = 0;
    
        getVote();
        
        $("#liked").click(function(){
            var vote;
            if($("#liked").html() == liked){
                vote = false;
                $("#liked").html('').prepend(voidLiked);
                $("#disliked").html('').prepend(disliked);  
            }else{
                vote = true;
                $("#liked").html('').prepend(liked);
                $("#disliked").html('').prepend(voidDisliked);
            }
            sendVote(vote);
        });
    
        $("#disliked").click(function(){
            var vote;
            if($("#disliked").html() == disliked){
                vote = true;
                $("#liked").html('').prepend(liked);
                $("#disliked").html('').prepend(voidDisliked);       
            }else{
                vote = false;
                $("#liked").html('').prepend(voidLiked);
                $("#disliked").html('').prepend(disliked);
            }
            sendVote(vote);
        });
    
        function getVote(){
            // get like kind from campaign_id
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": window.location.href.split("campaign")[0]+"like/"+campaign_id,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache"
                }
            }
              
            $.ajax(settings).done(function (response) {
                if(response.length == 0){
                    $( "#liked" ).append( voidLiked );
                    $( "#disliked" ).append( voidDisliked );
                }else{
                    like_id = response[0].id
                    if(response[0].kind){
                        $("#liked").html('').prepend(liked);
                        $("#disliked").html('').prepend(voidDisliked);
                    }else{
                        $("#disliked").html('').prepend(disliked);
                        $( "#liked" ).append( voidLiked );
                    }
                }
                
            });    
    
        }
    
        function sendVote(vote){
            if(like_id == 0){
                createVote(vote);
            }else{
                updateVote(vote);
            }
            
        }
    
        function createVote(vote){
            var data = JSON.stringify({"like": {"kind": vote,"campaign_id": parseInt(campaign_id)} });
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": window.location.href.split("campaign")[0]+"like",
                "method": "POST",
                "headers": {
                    "cache-control": "no-cache",
                    "Content-Type": "application/json"
                },
                "processData": false,
                "data": data
            }
    
            $.ajax(settings).done(function (response) {
                if(response.kind == vote){
                    like_id = response.id
                    alert("Created!");
                }else{
                    alert("Something went wrong!");
                }
            });    
        }
    
        function updateVote(vote){
            var data = JSON.stringify({"like": {"kind": vote} });
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": window.location.href.split("campaign")[0]+"like/"+like_id,
                "method": "PUT",
                "headers": {
                    "cache-control": "no-cache",
                    "Content-Type": "application/json"
                },
                "processData": false,
                "data": data
            }
    
            $.ajax(settings).done(function (response) {
                if(response.kind == vote){
                    like_id = response.id
                    alert("Updated!");
                }else{
                    alert("Something went wrong!");
                }
            });    
        }
    
    });
    ```

    > Essas funções são responsáveis por: atualizar o voto da campaign caso o usuário já tenha votado, pegar o evento click e realizar as trocas dos ícones e enviar a escolha do usuário, se gostou ou não gostou, para os métodos create ou update do controlador like.
    >
    > **Importante:** caso exista algum arquivo **.coffee** em **app/assets/javascripts**, excluí-los. 

39. Em **app/controllers/like_controller** substitua por:

    ```ruby
    class LikeController < ApplicationController
      before_action :set_campaign, only: [:show]
      before_action :set_like, only: [:update]
    
      def show
        @like = Like.where(campaign: @campaign, user: current_user)
        render json: @like
      end
    
      def create
        @like = Like.new(like_params.merge(user: current_user))
        if @like.save
          render json: @like
        else
          render json: @like.errors, status: :unprocessable_entity
        end
      end
    
      def update
        if @like.update(like_params)
          render json: @like
        else
          render json: @like.errors, status: :unprocessable_entity
        end
      end
    
      private
    
      def like_params
        params.require(:like).permit(:kind, :campaign_id)
      end
    
      def set_campaign
        @campaign = Campaign.find(params[:id])
      end
    
      def set_like
        @like = Like.find(params[:id])
      end
    
    end
    
    ```

40. Em **app/controllers/application_controller** troque por:

    ```ruby
    class ApplicationController < ActionController::Base
      before_action :configure_permitted_parameters, if: :devise_controller?
      before_action :authenticate_user!
      skip_before_action :verify_authenticity_token, if: :json_request?
    
      protected
    
      def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
      end
    
      def json_request?
        request.format.json?
      end
    
    end
    ```

    > Como estamos fazendo requisição via ajax é necessário adicionar *skip_before_action* para evitar o erro *InvalidAuthenticityToken*

41. Agora para finalizar vamos alterar o arquivo routes em **config/routes.rb** por:

    ```ruby
    Rails.application.routes.draw do
      devise_for :users
      mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
      root to: "campaigns#index"
      resources :campaigns, only: [:index, :show]
      resources :like, only: [:show, :create, :update]
    end
    ```

42. Execute o seguinte commando e abre o browser em localhost:3000 ou em localhost:3000/admin.

    ```ruby
    rails s
    ```



<h2>Conclusão</h2>

Vimos nesse passo a passo que o grande benefício do rails admin foi auxiliar a criação do sistema administrativo de maneira rápida e fácil, precisando apenas realizar algumas configurações e no final foi gerado um administrativo elegante, simples e funcional. Também vimos que com o auxilio do framework Bulma conseguimos criar, facilmente, interfaces bem elegantes.
